import React from 'react';
import { View, StyleSheet, Animated, Image } from 'react-native';
import MapView from 'react-native-maps';
import GeoCoder from 'react-native-geocoder';

export default class Main extends React.Component {
    constructor(props) {
        super(props);
        this.initialRegion = {
            // latitude: 13.7662233,
            latitude: 37.78825,
            // longitude: 100.60939580000002,
            longitude: -122.4324,
            latitudeDelta: 0.00922,
            longitudeDelta: 0.00421,
        };

        this.state = {
            position: null,
            carLocations: [
                {
                    rotation: 78,
                    latitude: 37.78825,
                    longitude: 122.4318,
                },
                {
                    rotation: -10,
                    latitude: 37.79015,
                    longitude: -122.4318,
                }
            ]
        }
    }

    _onRegionChange(region) {
        this.setState({
            position: null
        });
        
        const self = this;
        if(this.timeoutId) {
            clearTimeout(this.timeoutId);
        }
        this.timeoutId = setTimeout(async () => {
            try {
                const response = await GeoCoder.geocodePosition({
                    lat: region.latitude,
                    lag: resion.longitude
                });
                self.setState({position: response[0]})
            } catch(err) {
                console.log(err);
            }
        }, 2000);
    }

    componentDidMount() {
        this._onRegionChange.call(this, this.initialRegion);
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <MapView
                    style={styles.fullScreenMap}
                    initialRegion={this.initialRegion}
                    onRegionChange={this._onRegionChange.bind(this)}
                >
                    {
                        this.state.carLocations.map((carLocation, i) => {
                            <MapView.Marker key={i} coordinate={{
                                latitude: parseFloat(carLocation.latitude),
                                longitude: parseFloat(carLocation.longitude)
                            }}
                            image={require('../img/car.png')}
                            >
                                {/* <Animated.Image
                                    style={{
                                        transform: [
                                            {rotate: `${carLocation.rotation}deg`}
                                        ],
                                    }}
                                    source={require('../img/car.png')}
                                /> */}
                                
                            </MapView.Marker>
                        })
                    }
                </MapView>
                <Image source={require('../img/car.png')} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    fullScreenMap: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
    }
});
